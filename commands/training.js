const polyfill = require('babel-polyfill');

exports.run = async (client, message, args) => {
  if (message.author.bot) return
  const playerName = message.author.lastMessage.member.nickname

  try {
    message.channel.send(`*CeeFive brings up your personal shockball calendar...*`).catch(console.error);
    await client.firebase.collection('players').where('name', '==', playerName).get().then((snapshot) => {
      let players = []
      snapshot.forEach((doc) => {
        players.push(doc.data())
      })
      if (players.length > -1) {
        const player = players[0]
        message.channel.send(`*he shows you your current training regimens...*
        
          ====Training Queue====
          || \`Name\`: ${player.name}
          || 1 - ${player.trainingQueue.regimen1.value}
          || 2 - ${player.trainingQueue.regimen2.value}
          || 3 - ${player.trainingQueue.regimen3.value}
          || 4 - ${player.trainingQueue.regimen4.value}
          || 5 - ${player.trainingQueue.regimen5.value}
          || 6 - ${player.trainingQueue.regimen6.value}
          || 7 - ${player.trainingQueue.regimen7.value}
          ======================
        `).catch(console.error);
      } else {
        message.channel.send(`*he can't seem to find your calendar info in his datapad.*`).catch(console.error);
      }
    })
  } catch (error) {
    console.log(error);
    message.channel.send(`*he frowns at his datapad and thumps it in irritation. It's not working right now.*`).catch(console.error);
  }
}