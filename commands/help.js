exports.run = (client, message, args) => {
	message.channel.send(`Salutations, I am CeeFive, bartending droid of the reputable Shocker Gastropub. Here's what I can do:

	==Utilities
	\`ceefive help\`:   lists all of my commands
	\`ceefive ping\`:   get a confirmation response from me that I am powered on and working
	==Shockball Actions
	\`ceefive stats\`:   get information about your player
	\`ceefive training\`:   lists your player's current training queue
	==Social Actions
	\`ceefive hey\`:  chat with the bartender and about bartendery things (keywords being *menu* and drink names from the menu)
	  (example):  \`ceefive hey show me the menu\`
	  (example):  \`ceefive hey give me a corellian ale\`
	  (example):  \`ceefive hey good morning!\`
	`).catch(console.error);
}

// \`ceefive today\`:   lists matches happening today
// \`ceefive train . .\`:   set a specific training regimen on your player's 1-7 queue
// (example):  \`ceefive train 2 wing\`
// (example):  \`ceefive train 7 rest\`