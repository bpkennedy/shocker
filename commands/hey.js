const utils = require('../lib/utils.js');

exports.run = (client, message, args) => {
  if (message.author.bot) return

  const messageMinusPrefix = message.content.slice(client.config.prefix.length  + 4);
  const confirmationFilter = m => utils.confirmationResponse(m.content);
  const menu = `
    Of course, have a gander at our menu...
    \`\`\`
    ==Ale==
    Corellian Grog
    Tuskan Sandstorm
    ==Whiskey==
    Duro Spice Liquor
    Alderan Fire Water
    \`\`\`
  `;
  let ceefiveResponse = null;

  isDuroType = (message) => {
    if (message.includes('liquor') || message.includes('hard') || message.includes('stiff') || message.includes('whiskey') || message.includes('duro') || message.includes('spice')) {
      return true
    } else {
      return false
    }
  }

  isAlderanType = (message) => {
    if (message.includes('fire') || message.includes('water') || message.includes('special') || message.includes('alderan') || message.includes('alderannian') || message.includes('fire water')) {
      return true
    } else {
      return false
    }
  }

  isGrogType = (message) => {
    if (message.includes('grog') || message.includes('beer') || message.includes('ale') || message.includes('corellian') || message.includes('corellia') || message.includes('pint')) { 
      return true
    } else {
      return false
    }
  }

  isSandstormType = (message) => {
    if (message.includes('tuskan') || message.includes('tusken') || message.includes('sandstorm')) { 
      return true
    } else {
      return false
    }
  }

  if (messageMinusPrefix.indexOf('menu') > -1) {
    ceefiveResponse = menu;
  } else if (isGrogType(messageMinusPrefix)) {
    ceefiveResponse = `*CeeFive pours a cool draft of corellian grog into a tumbler.*`;
  } else if (isSandstormType(messageMinusPrefix)) {
    ceefiveResponse = `*CeeFive pops the cap on a frigid bottle of imported Tatoonian ale and hands it to you.*`;
  } else if (isDuroType(messageMinusPrefix)) {
    ceefiveResponse = `*CeeFive drops a shot glass on the counter, pours in some sparkling liquid, and slides it over to you.*`;
  } else if (isAlderanType(messageMinusPrefix)) {
    ceefiveResponse = `*CeeFive studies you carefully*    Are you sure about this?`;
    message.channel.send(ceefiveResponse)
    .then(() => {
      message.channel.awaitMessages(confirmationFilter, {
        max: 1,
        time: 30000,
        errors: ['time'],
      })
      .then((collected) => {
          if (utils.isAffirmative(collected.first().content)) {
            ceefiveResponse = `*CeeFive pours a shot of sizzling amber into a neon rimmed snifter and sets it in front of you.*`;
          } else {
            ceefiveResponse = `*CeeFive nods, unsurprised, and turns back to the bar.*`;
          }
          message.channel.send(ceefiveResponse);
        })
        .catch((error) => {
          ceefiveResponse = '*CeeFive shakes his head and moves on to his other duties*';
          message.channel.send(ceefiveResponse);
        });
    });

    return;
  } else if (utils.isGreeting(messageMinusPrefix)) {
    ceefiveResponse = utils.getGreetingResponse(messageMinusPrefix);
    if (!ceefiveResponse) {
      ceefiveResponse = '*CeeFive reviews you dispassionately and continues about his business.*'
    }
  } else {
    ceefiveResponse = `*CeeFive looks confused*`
  }

  message.channel.send(ceefiveResponse);

}