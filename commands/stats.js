const polyfill = require('babel-polyfill');

exports.run = async (client, message, args) => {
  if (message.author.bot) return
  let playerName = message.author.lastMessage.member.nickname
  try {
    message.channel.send(`*CeeFive queries the shockball league systems...*`).catch(console.error);
    await client.firebase.collection('players').where('name', '==', playerName).get().then((snapshot) => {
      let players = []
      snapshot.forEach((doc) => {
        players.push(doc.data())
      })
      if (players.length > -1) {
        const player = players[0]
        message.channel.send(`*he shows you his datapad...*
        
        ==Overall
        \`Name\`: ${player.name}
        \`Signed With\`: ${player.teamName}
        \`Overall Rating\`: ${player.overallRating}
        \`Center Rating\`: ${player.centerRating}
        \`Wing Rating\`: ${player.wingRating}
        \`Guard Rating\`: ${player.guardRating}
        ==Skills
        \`Throwing\`: ${player.throwing}
        \`Passing\`: ${player.passing}
        \`Blocking\`: ${player.blocking}
        \`Vision\`: ${player.vision}
        \`Endurance\`: ${player.endurance}
        \`Toughness\`: ${player.toughness}
        ==Modifiers
        \`Leadership\`: ${player.leadership}
        \`Energy\`: ${player.energy}
        \`Morale\`: ${player.morale}
        \`Aggression\`: ${player.aggression}
        `).catch(console.error);
      } else {
        message.channel.send(`*he types into his datapad but can't seem to find ${playerName} in the system.*`).catch(console.error);
      }
    })
  } catch (error) {
    console.log(error);
    message.channel.send(`*he frowns at his datapad and thumps it in irritation. It's not working right now.*`).catch(console.error);
  }
}