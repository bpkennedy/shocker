// import 3rd party libs
const sinon = require('sinon');

// import our file under test
const hey = require('../commands/hey.js');
const clientConfig = {
  config: {
    prefix: 'ceefive'
  }
};

test('ceefive should try sending a response to channel when you call him by prefix and hey command', () => {
  // prepare
  const messageConfig = {
    content: 'ceefive hey someString',
    authorBot: false,
    channelSend: sinon.spy()
  };
  const messageMock = require('./mockMessage')(messageConfig);
  
  // act
  hey.run(clientConfig, messageMock);  
  
  // assert
  expect(messageConfig.channelSend.called).toBe(true);
});

test('ceefive should respond with the menu if message contains the word menu', () => {
  // prepare
  const messageConfig = {
    content: 'ceefive hey show me the menu',
    authorBot: false,
    channelSend: sinon.spy()
  };
  const menu = `
    Of course, have a gander at our menu...
    \`\`\`
    ==Ale==
    Corellian Grog
    Tuskan Sandstorm
    ==Whiskey==
    Duro Spice Liquor
    Alderan Fire Water
    \`\`\`
  `;
  const messageMock = require('./mockMessage')(messageConfig);
  
  // act
  hey.run(clientConfig, messageMock);  
  
  // assert
  expect(messageConfig.channelSend.calledWith(menu)).toBe(true);
});
