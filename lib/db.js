const polyfill = require('babel-polyfill')
const admin = require('firebase-admin')

let serviceAccount
if (!process.env.FIREBASE_DATABASE_URL) {
    serviceAccount = require('../firebase-security.json')
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: 'https://swc-shockball3.firebaseio.com/'
    })
} else {
    admin.initializeApp({
        credential: admin.credential.cert({
            'projectId': process.env.FIREBASE_PROJECT_ID,
            'clientEmail': process.env.FIREBASE_CLIENT_EMAIL,
            'privateKey': process.env.FIREBASE_PRIVATE_KEY,
        }),
        databaseURL: process.env.FIREBASE_DATABASE_URL
    })
}

function initialize() {
    let db = admin.firestore()
    return db
}

module.exports = {
    initialize
}