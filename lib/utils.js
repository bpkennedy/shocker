function confirmationResponse(s) {
  if (isAffirmative(s)) {
    return true;
  } else if (isNegative(s)) {
    return true;
  } else {
    return false;
  }
}

function isAffirmative(s) {
  let isAffirmative = false;
  const affirmativeWords = ['yes', 'yep', 'yup', 'yea', 'yeppers', 'uh huh', 'right', 'correct', 'indeed', 'sure', 'surely', 'alright'];
  affirmativeWords.some(function(word) {
    if (s.indexOf(word) > -1) {
      isAffirmative = true;
      return isAffirmative;
    }
  });
  return isAffirmative;
}

function isNegative(s) {
  let isNegative = false;
  const negativeWords = ['nope', 'incorrect', 'negative', 'nevermind', 'cancel', 'stop', 'end', 'terminate', 'quit', 'not', 'no'];
  negativeWords.some(function(word) {
    if (s.indexOf(word) > -1) {
      isNegative = true;
      return isNegative;
    }
  });
  return isNegative;
}

function isGreeting(s) {
  const nonSpecialCharString = s.replace(/[^\w\s]/gi, '')
  const words = nonSpecialCharString.split(' ');
  const greetingWords = {
    'hello': 'Hello, sentient.',
    'morning': 'Indeed, a fine morning!',
    'evening': 'Good evening to you.',
    'afternoon': 'Good day, sentient. Feeling thirsty?',
    'how are you': `I'm very well, thank you.`,
    'good night': 'Goodnight, sentient.',
    'later': 'Another time, perhaps.',
    'bye': 'Farewell, come back soon!',
    'hi': 'Good day.',
    'yo': 'A jolly hello to you too, sentient.',
    'hey': 'Hello.'
  }
  for (let word of words) {
    // this checks to see if the word is a partial match to the object property keys.
    if(Object.keys(greetingWords).some(key => {return key.indexOf(word) > -1 })){
      return true
    } else {
      return false
    }
  }
  
}

function getGreetingResponse(s) {
  const nonSpecialCharString = s.replace(/[^\w\s]/gi, '')
  var words = nonSpecialCharString.split(' ');
  let greetingResponse = '';
  const greetingWords = {
    'hello': 'Hello, sentient.',
    'morning': 'Indeed, a fine morning!',
    'evening': 'Good evening to you.',
    'afternoon': 'Good day, sentient. Feeling thirsty?',
    'how are you': `I'm very well, thank you.`,
    'good night': 'Goodnight, sentient.',
    'later': 'Another time, perhaps.',
    'bye': 'Farewell, come back soon!',
    'hi': 'Good day.',
    'yo': 'A jolly hello to you too, sentient.',
    'hey': 'Hello.'
  }
  
  for (let word of words) {
    let keyWithString = Object.keys(greetingWords).filter(key => key.indexOf(word) > -1);
    greetingResponse = greetingWords[keyWithString];
  }
  
  return greetingResponse;
}

module.exports = {
  confirmationResponse,
  isAffirmative,
  isNegative,
  isGreeting,
  getGreetingResponse
}